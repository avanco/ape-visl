#!/usr/bin/env perl

#
#	APE - Automatic Post-Editor
#
#	APE parses flat structure extended by dependency links
#	from PALAVRAS and apply rules for nominal and verbal concordance
#

use Data::Dumper;
use warnings;
use strict;
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname(abs_path $0).'/lib';
use H::GeraSuperficial qw(gera_superficial);
# 			$new_tok = gera_superficial("lemma=$tok_lemma", "pos=det", "gender=$tok_gender", "number=$tok_number", "type=$tok_type");

print "SENTENCE: $ARGV[0]\n";
open FLAT, "<", $ARGV[0] or die $!;
chomp (my @flat_words = <FLAT>);
# a hash used inside this sub. verbal: aims store subj-verb distance
my %dist_subjs = ();


sub nominal_verbal_rules {
	my @words = @{shift;};
=pod
	foreach (@words) {
		&nominal($_);
	}
=cut
	foreach (@words) {
		&verbal($_);
	}
}

# check nominal concordance
sub nominal {
	my @words = @{shift;};
	my $mod_gender, my $mod_number;
	my $head_gender, my $head_number;
	if ($words[0] =~ /\@>N|\@N<|\@<PRED|\@<SC/ and $words[0] =~ /DET|ADJ/
		and $words[1] !~ /\sV\s/) {
		print Dumper @words;
		if ($words[0] =~ /(\w\/?\w?)\s(\w)\s\@/) {
			$mod_gender = $1;
			$mod_number = $2;
		}
		if ($words[1] =~ /(\w\/?\w?)\s(\w)\s\@/) {
			$head_gender = $1;
			$head_number = $2;
		}
		# TODO: S/P number
		if ($mod_number ne "S/P" and $head_number ne "S/P" and $mod_number ne $head_number) {
			print "---> CORRIGIR (numero): $words[0]\n\n";
		}
		if ($mod_gender ne "M/F" and $head_gender ne "M/F" and $mod_gender ne $head_gender) {
			# PS: PALAVRAS does not set comparison adjectives, like 'maior' or 'menor', as M/F gender
			if ($words[0] !~ /<KOMP>/) {
				print "---> CORRIGIR (genero): $words[0]\n\n";
			}
		}
	}
}

# input: one pair of linked words
# check verbal concordance
sub verbal {
	my @words = @{shift;};
	# get out of subroutine if word0 or word1 is not a real word (@, -, ...)
	return if $words[0] !~ /^[A-Za-zÀ-ú_]+/ or $words[1] !~ /^[A-Za-zÀ-ú_]+/;
	##
	if ($words[0] =~ /\@<?SUBJ>?/ and $words[1] =~ /\sV\s/) {
		# update hash of distance subj <-> verb
		my $subj_id, my $verb_id;
		if ($words[0] =~ /\#(\d+)->(\d+)/) {
			$subj_id = $1;
			$verb_id = $2;
			if (exists $dist_subjs{$verb_id}) {
				$dist_subjs{$verb_id} = $subj_id-$verb_id
					if abs($subj_id-$verb_id) < abs($dist_subjs{$verb_id});
			}
			else {
				$dist_subjs{$verb_id} = $subj_id-$verb_id;
			}
		}
	} elsif ($words[1] =~ /\@<?SUBJ>?/ and $words[0] =~ /\sV\s/) {
		# update hash of distance subj <-> verb, BUT here V points to SUBJ
		my $subj_id, my $verb_id;
		if ($words[0] =~ /\#(\d+)->(\d+)/) {
			# BUT here V points to SUBJ
			$verb_id = $1;
			$subj_id = $2;
			# cmp old distance between SUBJ-V and new distance between new_SUBJ-V
			if (exists $dist_subjs{$verb_id}) {
				$dist_subjs{$verb_id} =  $subj_id-$verb_id
					if abs($subj_id-$verb_id) < abs($dist_subjs{$verb_id});
			}
			else {
				$dist_subjs{$verb_id} = $subj_id-$verb_id;
			}
		}
	}
}

# input: one pair for checking concordance
sub check_verbal {
	my @words = @{shift;};
	my @all_words = @{shift;};

	# --> go out when...
	if ($words[1] =~ /SPEC/) {
		print "rel. pronoum: leave concordance check\n";
		return;
	}
	# --> go out when...
	my @punc_breaks_ids = ();
	my @punc_breaks = grep {/TRAVESSAO/} @all_words;
	foreach (@punc_breaks) {
		push @punc_breaks_ids, $1 if /\#(\d+)/;
	}
	my $id = $1 if $words[0] =~ /\#(\d+)/;
	my $id_head = $1 if $words[1] =~ /\#(\d+)/;
	foreach (@punc_breaks_ids) {
		if ($_ > $id && $_ < $id_head or $_ < $id && $_ > $id_head) {
			print "punc. break: leave concordance check\n";
			return;
		}
	}

	### --> concordance check begins

	my $verb_number, my $verb_gender;
	my $head_number, my $head_gender;
	print Dumper @words;
	# head
	if ($words[1] =~ /(\w\/?\w?)\s(\w\/?\w?)\s@<?SUBJ>?/) {
		$head_gender = $1;
		$head_number = $2;
	} elsif ($words[1] =~ /PERS\s(\w\/?\w?)\s[1|3](\w\/?\w?)/) {
		$head_gender = $1;
		$head_number = $2;
	}
	# verb
	if ($words[0] =~ /[1|3]([S|P])/) {
		$verb_number = $1;
	} elsif ($words[0] =~ /PCP\s(\w)\s([S|P])\s\@/) {
		$verb_gender = $1;
		$verb_number = $2;
	}
	print "head_number: $head_number\n" if defined $head_number;
	print "head_gender: $head_gender\n" if defined $head_gender;
	print "verb_number: $verb_number\n" if defined $verb_number;
	print "verb_gender: $verb_gender\n" if defined $verb_gender;
	# check number
	if (defined $head_number && defined $verb_number &&
		$head_number ne 'S/P' && $verb_number ne 'S/P' &&
		$head_number ne $verb_number) {
		print "CORRIGIR numero\n";
	}
	# check gender (PCP)
	if (defined $head_gender && defined $verb_gender &&
		$head_gender ne 'M/F' && $verb_gender ne 'M/F' &&
		$head_gender ne $verb_gender) {
		print "CORRIGIR genero\n";
	}
	print "\n\n";
}

# input: hash of subj - verb distance (hash reference)
# this sub. maps a hash ID=>DIST to ID_VERB=>ID_SUBJ
sub dist2id {
	my %dist = %{shift;};
	while (my($k,$v) = each %dist) {
		$dist{$k} = $k+$v;
	}
	return \%dist;
}

sub print_hash {
	my %h = %{shift;};
	while (my($k,$v) = each %h) {
		print "$k => $v\n";
	}
}

# input: array ref. of complete one flat sentence
# output: array of word pairs
sub mount_pairs {
	my @flat = @{shift;};
	my @pairs = ();
	foreach (@flat) {
		my $id, my $id_head;
		if (/\#(\d+)->(\d+)/) { $id=$1; $id_head=$2; }
		if ($id_head != 0) {
			push @pairs, [$flat[$id-1],$flat[$id_head-1]];
		}
	}
	return \@pairs;
}

# input: a word pair, id, id_head, that must be checked; and flat words too
# output: array of flat word pair
sub id2word {
	my $id = shift;
	my $id_head = shift;
	my @flat_words = @{shift;};
	my @words_pair = ();
	push @words_pair, $flat_words[$id-1];
	push @words_pair, $flat_words[$id_head-1];
	return \@words_pair;
}

sub main {
	my @flats, my @tmp_flat = ();
	foreach (@flat_words) {
		if (/<\/s>/) {
			push @flats, [@tmp_flat];
			@tmp_flat = ();
		}
		else {
			push @tmp_flat, $_;
		}
	}
	foreach (@flats) {
		print "ANOTHER Flat\n";
		my $ref_pairs = &mount_pairs($_);
		# all word pairs are submitted to nominal/verbal subs.
		&nominal_verbal_rules($ref_pairs);
		my %correct = %{&dist2id(\%dist_subjs);};
		# now we have id,id_head for pair concordance
		while (my($k,$v) = each %correct) {
			my @flat_pair = @{&id2word($k,$v,$_)};
			&check_verbal(\@flat_pair, $_);
		}
		%dist_subjs = ();
	}
	print "END SENTENCE: $ARGV[0]\n";
}

&main;
