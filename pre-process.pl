#!/usr/bin/env perl

use Data::Dumper;

#	Pre-Process flat data
#	Break flat when </s> is found

open FLAT, "<", $ARGV[0] or die $!;
chomp (@flat_file = <FLAT>);
@flats;
@tmp_flat = ();

foreach (@flat_file) {
	if (/<\/s>/) {
		push @flats, [@tmp_flat];
		@tmp_flat = ();
	}
	else {
		push @tmp_flat, $_;
	}
}

print Dumper @flats;
