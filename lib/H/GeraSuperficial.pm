package H::GeraSuperficial;

use strict;
use warnings;
use locale;

use Exporter qw(import);
our @EXPORT_OK = qw(gera_superficial);


# Programa gera_superficiais.pl

# BREVE DESCRICAO
# Esse programa converte uma sequencia de lema e informacoes morfossintaticas na forma superficial 
# correspondente. Para tanto, eh usado o gerador de formas superficias compilado
# com base no dicionario morfologico no formato de Apertium (original do Apertium-pt aumentado com 
# entradas de Unitex-pt no Projeto ReTraTos): o pt_gerador.bin. 

# ENTRADA
#  string contendo lema, PoS e informacoes morfologicas
# IMPORTANTE: As etiquetas da string de entrada devem ser as do PorTAl (veja arquivo Tags.pdf)

# SAIDA
#  Imprime na tela a forma superficial correspondente as informacoes passadas como entrada

# Exemplos de uso no fim deste arquivo


sub gera_superficial {
	# Verifica se foi passado o parametro obrigatorio, senao imprime modo de usar o programa
=pod
	if ($#@_ < 0) {
		print "Uso: $0 <lista_valores>\n";
		print "Exemplos:\n\tperl $0 lemma=criação pos=n number=pl gender=f\n\tcriações";
		 print "\n\tperl $0 lemma=\"São Paulo\" pos=np type=loc";
		print "\n\tperl $0 lemma=ser pos=v person=p3 number=sg form=ind time=futpres\n\tserá";
		print "\n\tperl $0 lemma=se pos=prn type=pro,ref person=p3 number=sp gender=mf\n\tse\n\n";
		exit;
	};
=cut
	my %valores = ();
	my ($c,$v);

=pod
	while ($#@_ >= 0) {
		#print "lendo arg = $ARGV[0]\n";
		($c,$v) = split(/=/,shift(@_));
		$valores{$c} = $v;
		#print "armazenando $c = $v\n";
	}
=cut
	foreach (@_) {
		($c,$v) = split(/=/,$_);
		$valores{$c} = $v;
	}

	my($base,$superficial);

		# Monta a string a ser passada para o gerador de formas superficiais

		$base = '^'.$valores{'lemma'}.'<'.$valores{'pos'}.'>';
		# ADJETIVO e SUBSTANTIVO 	
		# Podem ter como atributos: tipo (opcional), genero e numero
		if (($valores{'pos'} eq 'adj') || ($valores{'pos'} eq 'n')) {
			# tipo
			$base .= defined($valores{'type'}) ? '<'.$valores{'type'}.'>' : '';
			# genero
			$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
			# numero
			$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';
		}
		# NUMERAL
		# Pode ter como atributos: genero e numero
		elsif ($valores{'pos'} eq 'num') {
			# genero
			$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
			# numero
			$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';
		}
		# ADVERBIO e NOME PROPRIO
		# Podem ter como atributos: tipo (opcional)
		elsif (($valores{'pos'} eq 'adv') || ($valores{'pos'} eq 'np')) {
			# tipo
			$base .= defined($valores{'type'}) ? '<'.$valores{'type'}.'>' : '';
		}
		elsif ($valores{'pos'} eq 'det') {
			# tipo
			$base .= defined($valores{'type'}) ? '<'.$valores{'type'}.'>' : '';
			# genero
			$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
			# numero
			$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';
		}
		# PRONOME
		# Pode ter como atributos: tipo (1 ou 2), pessoa (opcional), genero e numero	
		elsif ($valores{'pos'} eq 'prn') {
			# tipo - pronome pode ter varios tipos separados por ','
			# tipos de pronomes em que isso ocorre: pos (possessivo), ref (reflexivo - en)
			# em ambos os casos 'pos' e 'ref' devem ser impressos como o segundo (ultimo) tipo
			if (defined($valores{'type'})) {
				if ($valores{'type'} =~ /,/) {
					my @tipos = split(/,/,$valores{'type'});
					if (($tipos[0] eq 'pos') || ($tipos[0] eq 'ref')) {
						$base .= '<'.$tipos[1].'><'.$tipos[0].'>';
					} 
					else { $base .= '<'.$tipos[0].'><'.$tipos[1].'>'; }
				}
				else {
					$base .= '<'.$valores{'type'}.'>';
				}
			}
			# pessoa
			$base .= defined($valores{'person'}) ? '<'.$valores{'person'}.'>' : '';
			# genero
			$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
			# numero
			$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';	
		}
		# RELATIVO
		# Pode ter como atributos: tipo, genero (opcional) e numero	(opcional)
		elsif ($valores{'pos'} eq 'rel') {
			# tipo
			$base .= defined($valores{'type'}) ? '<'.$valores{'type'}.'>' : '';
			# genero
			$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
			# numero
			$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';	
		}
		# VERBO
		# Pode ter como atributos: forma+tempo, pessoa (opcional), genero (opcional) e numero (opcional)	
		elsif (($valores{'pos'} eq 'v') || ($valores{'pos'} eq 'vaux')) {
			# mapeia form e time para uma unica etiqueta
			if (defined($valores{'form'})) {
				if (defined($valores{'time'})) { $base .= mapeia($valores{'form'},$valores{'time'}); }
				else { $base .= '<'.$valores{'form'}.'>'; }
				if (($valores{'form'} ne 'ger') || ($valores{'form'} ne 'inf')) {
					if ($valores{'form'} eq 'pp') {
						# genero
						$base .= defined($valores{'gender'}) ? '<'.$valores{'gender'}.'>' : '';
					}
					else {
						# pessoa
						$base .= defined($valores{'person'}) ? '<'.$valores{'person'}.'>' : '';
					}
					# numero
					$base .= defined($valores{'number'}) ? '<'.$valores{'number'}.'>' : '';		
				}
			}
		}	
		# cnjadv, cnjcoo, cnjsub, ij, pr, rel - PoS que nao possui atributos
		
		$base .= '$';	# formata a entrada para a analise

	#	print "Base = $base\n";

		# Chama pt_gerador.bin (por meio de lt-proc) para gerar a forma superficial correspondente a $analise
		open(IN,"echo \"$base\" | lt-proc -g pt_gerador.bin|"); # -g: marca palavras desconhecidas com #, -n: nao marca palavras desconhecidas
		$superficial = <IN>;
		close IN;

		#print "$superficial\n";
		$superficial
}

# ************************************************* SUB-ROTINAS ********************************************** #

# Mapeia atributos do PorTAl para padrao do Apertium
# form=ind time=futpret => cni 
# form=ind time=futpres => fti
# form=ind time=pretimp => pii
# form=ind time=pretmqp => pmp
# form=ind time=pretperf => ifi
# form=ind time=pres => pri
# form=subj time=prs => pres
# form=subj time=pretimp => pis
# form=subj time=fut => fts
sub mapeia {
	my($form,$time) = @_;
 
	if (($form eq "ind") && ($time eq "futpret")) { return "<cni>"; }
	if (($form eq "ind") && ($time eq "futpres")) { return "<fti>"; }
	if (($form eq "ind") && ($time eq "pretimp")) { return "<pii>"; }
	if (($form eq "ind") && ($time eq "pretmqp")) { return "<pmp>"; }
	if (($form eq "ind") && ($time eq "pretperf")) { return "<ifi>"; }	
	if (($form eq "ind") && ($time eq "pres")) { return "<pri>"; }
	if (($form eq "subj") && ($time eq "pres")) { return "<prs>"; }
	if (($form eq "subj") && ($time eq "pretimp")) { return "<pis>"; }
	if (($form eq "subj") && ($time eq "fut")) { return "<fts>"; }
	return "";
}

# ******************************************************************************************************* #
# EXEMPLOS DE LINHA DE COMANDO PARA CADA CATEGORIA (pos):
# ADJETIVO
# $ perl gera_superficial.pl lemma=difícil pos=adj gender=mf number=pl
# difíceis

# ADVERBIO
# $ perl gera_superficial.pl lemma=difícil pos=adj gender=m number=pl type=sup
# dificílimos/dificilíssimos

# 	=> Veja que nesse caso existem duas formas possiveis de formas superficiais para os parametros
# passados para o gerador. As opcoes sao apresentadas com uma barra (/) entre elas

# $ perl gera_superficial.pl lemma="por conseguinte" pos=adv
# por conseguinte

# CONJUNCAO ADVERBIAL
# $ perl gera_superficial.pl lemma=se pos=cnjadv
# se

# CONJUNCAO COORDENATIVA
# $ perl gera_superficial.pl lemma=e pos=cnjcoo
# e

# CONJUNCAO SUBORDINATIVA
# $ perl gera_superficial.pl lemma=que pos=cnjsub
# que

# DETERMINANTE
# $ perl gera_superficial.pl lemma=o pos=det gender=f number=pl type=def
# as

# INTERJEICAO
# $ perl gera_superficial.pl lemma=ai pos=ij
# ai

# SUBSTANTIVO
# $ perl gera_superficial.pl lemma=casa pos=n gender=f number=pl type=dim
# casinhas/casitas/casuchas/casinholas/casinhotas

# NOME PROPRIO
# $ perl gera_superficial.pl lemma=Ana pos=np type=ant
# Ana

# $ perl gera_superficial.pl lemma="São Paulo" pos=np type=loc
# São Paulo

#	=> Nesse caso a inicial deve estar maiuscula para que seja encontrada no dicionario morfologico

# NUMERAL
# $ perl gera_superficial.pl lemma=dois pos=num gender=f number=sp
# duas

# PREPOSICAO
# $ perl gera_superficial.pl lemma=para pos=pr
# para

# PRONOME
# $ perl gera_superficial.pl lemma=quem pos=prn type=itg gender=mf number=sp
# quem

# $ perl gera_superficial.pl lemma=meu pos=prn type=pos,tn gender=f number=pl
# minhas

# $ perl gera_superficial.pl lemma=meu pos=prn type=tn,pos gender=f number=pl
# minhas

# $ perl gera_superficial.pl lemma=eu pos=prn type=tn person=p1 gender=mf number=sg
# eu

# $ perl gera_superficial.pl lemma=nós pos=prn type=tn person=p1 gender=mf number=pl
# nós

# 	=> Cuidado ao tentar transformar um pronome em outro. Veja que no dicionario o lema 
# de eu eh "eu" e o de nós, "nós"

# RELATIVO
# $ perl gera_superficial.pl lemma=qual pos=rel type=adv
# qual

# $ perl gera_superficial.pl lemma="o qual" pos=rel type=an gender=f number=pl
# as quais

# VERBO
# $ perl gera_superficial.pl lemma=escrever pos=v form=ind time=pres person=p1 number=sg
# escrevo

# $ perl gera_superficial.pl lemma=escrever pos=v form=ind time=pres person=p2 number=sg
# escreves

# $ perl gera_superficial.pl lemma=escrever pos=v form=ind time=pretimp person=p1 number=sg
# escrevia

# $ perl gera_superficial.pl lemma=escrever pos=v form=pp gender=f number=sg
# escrita

# $ perl gera_superficial.pl lemma=escrever pos=v form=ger
# escrevendo

1;
